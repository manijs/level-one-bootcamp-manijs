#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct payroll
{
    char **employees;
    float *hourly_wage;
    int m;
    char **employees_paycard;
    int *worked_time;
};

struct filtered
{
    char **employees;
    float *hours_worked;
    float *hourly_wage;
    int n;
};

int get_n()
{
    int n;
    scanf("%d", &n);
    return n;
}

struct payroll get_input(int n)
{
    struct payroll paycards;
    paycards.employees = malloc(n * sizeof(char *));
    paycards.hourly_wage = malloc(n * sizeof(float));
    for (int i = 0; i < n; i++)
    {
        paycards.employees[i] = malloc(sizeof(char) * 10);
        scanf("%s", paycards.employees[i]);
        scanf("%f", &paycards.hourly_wage[i]);
    }
    scanf("%d", &paycards.m);
    paycards.worked_time = malloc(sizeof(int) * paycards.m);
    paycards.employees_paycard = malloc(sizeof(char *) * paycards.m);
    for (int i = 0; i < paycards.m; i++)
    {
        paycards.employees_paycard[i] = malloc(sizeof(char) * 10);
        scanf("%s", paycards.employees_paycard[i]);
        scanf("%d", &paycards.worked_time[i]);
    }
    return paycards;
}

float *get_total_time(struct payroll input, int n)
{
    float *total_time;
    total_time = malloc(sizeof(int) * n);
    int j = 0;
    for (int i = 0; i < n; i++)
    {
        total_time[i] = input.worked_time[j];
        while (j < input.m)
        {
            if (j != input.m - 1)
            {
                if (strcmp(input.employees_paycard[j], input.employees_paycard[j + 1]) == 0)
                {
                    total_time[i] += input.worked_time[j + 1];
                    j++;
                }
                else
                {
                    j++;
                    break;
                }
            }
            else
                j++;
        }
    }
    return total_time;
}

float* remove_zeros(float* x,int n)
{
    for(int j = 0;j<n;j++)
    {
        if(x[j] == -1)
        {   
            for (int i = j; i < n-1; i++)
            {
                x[i] = x[i+1];
            }
        }
    }
    return x;
}

char** find_null_names(char** x,int n)
{
    for(int j = 0;j<n;j++)
    {
        if(strcmp(x[j],"NULL") == 0)
        {   
            for (int i = j; i < n-1; i++)
            {
                x[i] = x[i+1];
            }
        }
    }
    return x;
}

struct filtered get_filered_data(float *worked_minutes,struct payroll input,int n)
{
    struct filtered final_data;
    final_data.hours_worked = malloc(sizeof(float)*n);
    final_data.employees = malloc(sizeof(char*)*n);
    final_data.hourly_wage = input.hourly_wage;
    final_data.n = n;
    for(int i = 0 ; i < n ; i++ )
    {
        final_data.hours_worked[i] = worked_minutes[i]/60;
        final_data.employees[i] = malloc(sizeof(char)*10);
        int bool = 0;
        for(int j = 0 ; j < input.m ; j++)
        {
            
            if(strcmp(input.employees[i],input.employees_paycard[j]) == 0)
            {
                bool = 1;
                final_data.employees[i] = input.employees[i];
                break;
            }
        }
        if(bool != 1)
        {
            final_data.employees[i] ="NULL";
            final_data.n = final_data.n - 1;
            final_data.hourly_wage[i] = -1; 
        }
    }
    final_data.employees = find_null_names(final_data.employees,n);
    final_data.hourly_wage = remove_zeros(final_data.hourly_wage,n);
    return final_data;
}

float *get_salary(struct filtered data)
{
    float *salary = malloc(sizeof(float)*data.n);
    for(int i = 0 ; i < data.n ;i++)
    {
        if (data.hours_worked[i] <= 40)
            salary[i] = data.hours_worked[i]*data.hourly_wage[i];
        else if(data.hours_worked[i] > 40)
    	    salary[i] = (40* data.hourly_wage[i] + (data.hours_worked[i]-40)*1.5*data.hourly_wage[i]);
    }
    return salary;
}

void get_output(struct filtered data,float *salary)
{
    for (int i = 0; i < data.n; i++)
    {
        printf("%s: %.2f hours, $%.2f\n",data.employees[i],data.hours_worked[i],salary[i]);
    }
}

int main()
{
    struct payroll input;
    struct filtered final_data;
    float *total_time;
    float *salary;
    int n = get_n();
    input = get_input(n);
    total_time = get_total_time(input, n);
    final_data = get_filered_data(total_time,input,n);
    salary = get_salary(final_data);
    get_output(final_data,salary);
}
