#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct co_ordinates
{
    float x1,y1,x2,y2,x3,y3;
};

struct length_breadth
{
    float length,breadth;
};

int get_n()
{
    int n;
    scanf("%d",&n);
    return n;
}

struct co_ordinates *get_input(int n)
{
    struct co_ordinates *points;
    points = malloc(n*sizeof(struct co_ordinates));
    for(int i = 0 ; i < n ; i++)
    {
        scanf("%f%f%f%f%f%f",&points[i].x1,&points[i].y1,&points[i].x2,&points[i].y2,&points[i].x3,&points[i].y3);
    }
    return points;
}

int max_finder(float *l)
{
    float max;
    max = l[0];
    for(int j=0;j<3;j++)
    {
        if(l[j] >= max)
        {
            max = l[j];
        }
    }
    int i;
    for(i=0;i<3;i++)
        if(l[i] == max)
            break;
    return i;
}

struct length_breadth *get_length_and_breadth(struct co_ordinates *x,int n)
{
    struct length_breadth *l_and_b;
    l_and_b = malloc(sizeof(struct length_breadth)*n);
    for(int i = 0 ; i < n ; i++)
    {
        int f;
        float *l = malloc(sizeof(float)*3);
        l[0] = sqrt(pow((x[i].x3-x[i].x1),2)+(pow((x[i].y3-x[i].y1),2)));
        l[1] = sqrt(pow((x[i].x1-x[i].x2),2)+(pow((x[i].y1-x[i].y2),2)));
        l[2] = sqrt(pow((x[i].x3-x[i].x2),2)+(pow((x[i].y3-x[i].y2),2))); 
        f = max_finder(l);
        for (int j=f; j<2; j++)
            l[j] = l[j+1];
        l_and_b[i].length = l[0];
        l_and_b[i].breadth = l[1];
    }
    return l_and_b;
}

float *get_area(struct length_breadth *x,float n)
{
    float *area = malloc(sizeof(float)*n);
    for(int i = 0 ; i < n ; i++)
        area[i] = x[i].length*x[i].breadth;
    return area;
}

void get_output(struct co_ordinates *x,float *y,int n)
{
    for(int i = 0 ; i < n ; i++)
        printf("Area of rectangle with vertices (%.1f,%.1f),(%.1f,%.1f),(%.1f,%.1f) is %.1f\n",x[i].x1,x[i].y1,x[i].x2,x[i].y2,x[i].x3,x[i].y3,y[i]);
}

void main()
{
    int n;
    float *area;
    struct co_ordinates *points;
    struct length_breadth *l_and_b;
    n = get_n();
    points = get_input(n);
    l_and_b = get_length_and_breadth(points,n);
    area = get_area(l_and_b,n);
    get_output(points,area,n);
    free(area);
    free(points);
    free(l_and_b);
}
