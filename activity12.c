#include <stdio.h>
#include <stdlib.h>

struct unit_fraction 
{
    int number_of_efractions;
    int *denominators;
};

struct result
{
    int numerator;
    int denominator;
};

int get_n()
{
    int n;
    scanf("%d",&n);
    return n;
}

struct unit_fraction *get_input(int n)
{
    struct unit_fraction *egyptian_fraction;
    egyptian_fraction = malloc(n * sizeof(struct unit_fraction));
    for(int i = 0 ; i < n ; i++)
    {
        scanf("%d",&(egyptian_fraction+i)->number_of_efractions);
        (egyptian_fraction+i)->denominators = malloc(sizeof(int)*(egyptian_fraction+i)->number_of_efractions);
        for(int j = 0 ; j < (egyptian_fraction+i)->number_of_efractions ; j++)
        {
            scanf("%d",&(egyptian_fraction+i)->denominators[j]);
        }
    }
    return egyptian_fraction;
}

int get_gcd(int x,int y)
{
    int gcd;
    for(int i=1; i <= x && i <= y; i++)
    {
        if(x%i==0 && y%i==0)
            gcd = i;
    }
    return gcd;
}

int *get_lcm(struct unit_fraction *x,int n)
{
    int *lcm;
    lcm = malloc(sizeof(int)*n);
    for(int i = 0 ; i < n ; i++)
    {
        lcm[i] = (x+i)->denominators[0];
        for(int j = 1 ; j < (x+i)->number_of_efractions ; j++)
        {
            lcm[i] = (x+i)->denominators[j]*lcm[i]/get_gcd((x+i)->denominators[j],lcm[i]);
        }
    }
    return lcm;
}

struct result *get_result(int *x,int n,struct unit_fraction *y)
{
    struct result *res;
    res = malloc(sizeof(struct result)*n);
    for(int i = 0 ; i < n ; i++ )
    {
        res[i].numerator = 0;
        res[i].denominator = x[i];
        for(int j = 0 ;j < (y+i)->number_of_efractions ;j++)
            res[i].numerator = res[i].numerator+(1)*(res[i].denominator/(y+i)->denominators[j]);
    }
    return res;
}

struct result *get_simplified(struct result *x,int n)
{
    struct result *final_result;
    final_result = malloc(sizeof(struct result)*n);
    for (int i = 0 ; i < n ; i++)
    {
        final_result[i].numerator = x[i].numerator/get_gcd(x[i].numerator,x[i].denominator);
        final_result[i].denominator = x[i].denominator/get_gcd(x[i].numerator,x[i].denominator);
    }
    return final_result;
}

void get_output(struct unit_fraction *x,struct result *y,int n)
{
    for(int i = 0 ; i < n ; i++)
    {
        for(int j = 0 ; j < x[i].number_of_efractions ; j++)
        {
            printf("1/%d ",x[i].denominators[j]);
            if(j < x[i].number_of_efractions - 1)
            {
                printf("+ ");
            }
            else if(j == x[i].number_of_efractions - 1)
            {
                printf("= ");
            }
        }
        printf("%d/%d",y[i].numerator,y[i].denominator);
        printf("\n");
    }
}

void main()
{
    int n;
    struct unit_fraction *input;
    int *lcm;
    struct result *raw_result;
    struct result *final_result;
    n = get_n();
    input = get_input(n);
    lcm = get_lcm(input,n);
    raw_result = get_result(lcm,n,input);
    final_result = get_simplified(raw_result,n);
    get_output(input,final_result,n);
    free(input);
    free(lcm);
    free(raw_result);
    free(final_result);
}

