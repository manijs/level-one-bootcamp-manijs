#include <stdio.h>
#include <stdlib.h>

struct grade_books
{
    int m,k;
    char c_name[20];
    char **s_name;
    float *w;
    int **marks;
};

struct normalized_marks
{
    int *added_marks;
};

struct student_result
{
    float *final_weighted_normalized;
    char **grade;
};

int get_n()
{
    int n;
    scanf("%d",&n);
    return n;
}

struct grade_books *get_input(int n)
{
    struct grade_books *x;
    x = (struct grade_books*)malloc(sizeof(struct grade_books)*n);
    for(int j = 0;j<n;j++)
    {   
        getchar();
        fgets((x+j)->c_name,18,stdin);
        scanf("%d%d",&(x+j)->m,&(x+j)->k);
        (x+j)->w = malloc(sizeof(float)*(x+j)->k);
        (x+j)->marks = malloc(sizeof(int*)*(x+j)->m);
        (x+j)->s_name = malloc(sizeof(char*)*((x+j)->m));
        for(int i=0;i<(x+j)->m;i++)
        {
            (x+j)->s_name[i]  =  malloc(sizeof(char)*10);
            (x+j)->marks[i] = malloc(sizeof(int)*(x+j)->k);
        }
        for(int i=0;i<(x+j)->k;i++)
        {
            scanf("%f",&(x+j)->w[i]);
        }
        for(int i=0;i<(x+j)->m;i++)
        {
            getchar();
            fgets((x+j)->s_name[i],9,stdin);
            for(int f=0;f<(x+j)->k;f++)
            {
                scanf("%d",&(x+j)->marks[i][f]);
            }
        }
        getchar();
    }
    return x;
}

float *get_total_weight(struct grade_books *x,int n)
{
    float *W;
    W = malloc(sizeof(float)*n);
    for(int i = 0 ; i < n ; i++)
        W[i] = 0;
    for(int i = 0 ; i < n ; i++)
    {
        for(int j = 0 ; j < (x+i)->k ; j++)
            W[i] += (x+i)->w[j]; 
    }
    return W;
}


struct normalized_marks *get_every_student_average(struct grade_books *x,float *y,int n)
{
    struct normalized_marks *stu_result;
    stu_result = malloc(sizeof(struct normalized_marks)*n);
    for(int i = 0; i < n ; i++)
    {
        (stu_result+i)->added_marks = malloc(sizeof(int)*(x+i)->m);
        for(int j = 0; j < (x+i)->m ; j++ )
        {
            (stu_result+i)->added_marks[j] = 0;
            for(int k = 0; k < (x+i)->k ; k++)
            {
                (stu_result+i)->added_marks[j] += (x+i)->marks[j][k]*(x+i)->w[k];
            }
        }
    }
    return stu_result;
}

struct student_result *get_grade(struct grade_books *x,struct student_result *y,int n)
{
    struct student_result *a;
    a = malloc(sizeof(struct student_result)*n);
    for(int i = 0 ; i < n ; i++)
    {
        (a+i)->grade = malloc(sizeof(char*)*(x+i)->m);
        for(int j = 0 ; j < (x+i)->m ; j++)
        {
            (a+i)->grade[j] = malloc(sizeof(char));
            if((y+i)->final_weighted_normalized[j] >= 0 && (y+i)->final_weighted_normalized[j] < 60)
                (a+i)->grade[j] = "F";
            else if((y+i)->final_weighted_normalized[j] >= 60 && (y+i)->final_weighted_normalized[j] < 70)
                (a+i)->grade[j] = "D";
            else if((y+i)->final_weighted_normalized[j] >= 70 && (y+i)->final_weighted_normalized[j] < 80)
                (a+i)->grade[j] = "C";
            else if((y+i)->final_weighted_normalized[j] >= 80 && (y+i)->final_weighted_normalized[j] < 90)
                (a+i)->grade[j] = "B";
            else 
                (a+i)->grade[j] = "A";
        }
    }
    return a;
}

struct student_result *get_weighted_and_normalized(struct grade_books *x,float *y,struct normalized_marks *z,int n)
{
    struct student_result *final_stu_result;
    struct student_result *grades;
    final_stu_result = malloc(sizeof(struct student_result)*n);
    for(int i = 0 ; i < n ; i++)
    {
        (final_stu_result+i)->final_weighted_normalized = malloc(sizeof(float)*(x+i)->m);
        for(int j = 0 ; j < (x+i)->m ; j++)
        {
            (final_stu_result+i)->final_weighted_normalized[j] = (z+i)->added_marks[j]/y[i];
        }
    }
    grades = get_grade(x,final_stu_result,n);
    for(int i = 0 ; i < n ; i++)
        (final_stu_result+i)->grade = (grades+i)->grade;
    return final_stu_result;
}

void get_output(struct grade_books *x,struct student_result *y,int n)
{
    for (int i = 0; i < n; i++)
    {
        printf("%s",(x+i)->c_name);
        for (int j = 0; j < (x+i)->m; j++)
        {
            printf("%s\t",(x+i)->s_name[j]);
            printf("%.2f %s\n",(y+i)->final_weighted_normalized[j],(y+i)->grade[j]);
        }
        printf("\n");
    }
    
}

void main()
{
    int n;
    float *W;
    struct grade_books *a;
    struct normalized_marks *b;
    struct student_result *c;
    n = get_n();
    a = get_input(n);
    W = get_total_weight(a,n);
    b = get_every_student_average(a,W,n);
    c = get_weighted_and_normalized(a,W,b,n);
    get_output(a,c,n);
    free(W);
    free(a);
    free(b);
    free(c);
}
